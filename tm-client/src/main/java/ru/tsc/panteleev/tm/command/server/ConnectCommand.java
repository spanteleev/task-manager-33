package ru.tsc.panteleev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;

import java.net.Socket;

public class ConnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "connect";

    @NotNull
    public static final String DESCRIPTION = "Connect to server";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        try {
            Socket socket = serviceLocator.getConnectionEndpointClient().connect();
            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
            serviceLocator.getProjectEndpointClient().setSocket(socket);
            serviceLocator.getTaskEndpointClient().setSocket(socket);
            serviceLocator.getProjectTaskEndpointClient().setSocket(socket);
            serviceLocator.getDomainEndpointClient().setSocket(socket);
            serviceLocator.getUserEndpointClient().setSocket(socket);
        } catch (@NotNull Exception e) {
            serviceLocator.getLoggerService().error(e);
        }
    }

    @Override
    public Role[] getRoles() {
        return null;
    }
}
