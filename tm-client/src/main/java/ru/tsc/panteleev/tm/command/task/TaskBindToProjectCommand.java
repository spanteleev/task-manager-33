package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.task.TaskBindToProjectRequest;
import ru.tsc.panteleev.tm.util.TerminalUtil;

public class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-bind-to-project";

    @NotNull
    public static final String DESCRIPTION = "Task bind to project.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        getProjectTaskEndpoint().bindToProjectTaskById(new TaskBindToProjectRequest(projectId, taskId));
    }

}
