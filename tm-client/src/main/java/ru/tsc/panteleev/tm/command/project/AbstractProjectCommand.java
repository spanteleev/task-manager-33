package ru.tsc.panteleev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.IProjectEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Project;
import ru.tsc.panteleev.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderProjects(@NotNull final List<Project> projects) {
        int index = 1;
        for (final Project project : projects) {
            if (project == null) continue;
            System.out.println(index + "." + project);
            index++;
        }
    }

    protected void showProject(@NotNull final Project project) {
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(project.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(project.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(project.getDateEnd()));
    }

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return serviceLocator.getProjectEndpointClient();
    }

    @NotNull
    protected IProjectTaskEndpoint getProjectTaskEndpoint() {
        return serviceLocator.getProjectTaskEndpointClient();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
