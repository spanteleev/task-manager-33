package ru.tsc.panteleev.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.endpoint.IAuthEndpointClient;
import ru.tsc.panteleev.tm.dto.request.user.UserLoginRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserProfileRequest;
import ru.tsc.panteleev.tm.dto.response.user.UserLoginResponse;
import ru.tsc.panteleev.tm.dto.response.user.UserLogoutResponse;
import ru.tsc.panteleev.tm.dto.response.user.UserProfileResponse;

@NoArgsConstructor
public class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @SneakyThrows
    public UserLoginResponse login(UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @Override
    @SneakyThrows
    public UserLogoutResponse logout(UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @Override
    @SneakyThrows
    public UserProfileResponse profile(UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }


}
