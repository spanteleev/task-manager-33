package ru.tsc.panteleev.tm.api.endpoint;

public interface IProjectTaskEndpointClient extends IEndpointClient, IProjectTaskEndpoint {
}
