package ru.tsc.panteleev.tm.api.endpoint;

public interface IProjectEndpointClient extends IEndpointClient, IProjectEndpoint {
}
