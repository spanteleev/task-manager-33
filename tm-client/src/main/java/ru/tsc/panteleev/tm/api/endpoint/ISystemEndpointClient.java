package ru.tsc.panteleev.tm.api.endpoint;

public interface ISystemEndpointClient extends IEndpointClient, ISystemEndpoint {
}
