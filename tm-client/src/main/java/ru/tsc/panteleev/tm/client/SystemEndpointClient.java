package ru.tsc.panteleev.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.endpoint.ISystemEndpointClient;
import ru.tsc.panteleev.tm.dto.request.system.ServerAboutRequest;
import ru.tsc.panteleev.tm.dto.request.system.ServerVersionRequest;
import ru.tsc.panteleev.tm.dto.response.system.ServerAboutResponse;
import ru.tsc.panteleev.tm.dto.response.system.ServerVersionResponse;

public class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(ServerAboutRequest request) {
        return call(request, ServerAboutResponse.class);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(ServerVersionRequest request) {
        return call(request, ServerVersionResponse.class);
    }

}
