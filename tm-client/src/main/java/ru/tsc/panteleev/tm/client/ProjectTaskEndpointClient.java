package ru.tsc.panteleev.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.endpoint.IProjectTaskEndpointClient;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;

@NoArgsConstructor
public class ProjectTaskEndpointClient extends AbstractEndpointClient implements IProjectTaskEndpointClient {

    public ProjectTaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskBindToProjectResponse bindToProjectTaskById(@NotNull TaskBindToProjectRequest request) {
        return call(request, TaskBindToProjectResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskUnbindToProjectResponse unbindToProjectTaskById(@NotNull TaskUnbindToProjectRequest request) {
        return call(request, TaskUnbindToProjectResponse.class);
    }

}
