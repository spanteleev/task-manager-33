package ru.tsc.panteleev.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.IProjectTaskEndpoint;
import ru.tsc.panteleev.tm.api.endpoint.ITaskEndpoint;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.enumerated.Status;
import ru.tsc.panteleev.tm.model.Task;
import ru.tsc.panteleev.tm.util.DateUtil;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            if (task == null) continue;
            System.out.println(index + "." + task);
            index++;
        }
    }

    protected void showTask(@NotNull final Task task) {
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
        System.out.println("CREATED: " + DateUtil.toString(task.getCreated()));
        System.out.println("DATE BEGIN: " + DateUtil.toString(task.getDateBegin()));
        System.out.println("DATE END: " + DateUtil.toString(task.getDateEnd()));
    }

    @NotNull
    protected ITaskEndpoint getTaskEndpoint() {
        return serviceLocator.getTaskEndpointClient();
    }

    @NotNull
    protected IProjectTaskEndpoint getProjectTaskEndpoint() {
        return serviceLocator.getProjectTaskEndpointClient();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
