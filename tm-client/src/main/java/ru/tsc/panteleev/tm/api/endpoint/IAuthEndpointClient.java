package ru.tsc.panteleev.tm.api.endpoint;

public interface IAuthEndpointClient extends IEndpointClient, IAuthEndpoint {
}
