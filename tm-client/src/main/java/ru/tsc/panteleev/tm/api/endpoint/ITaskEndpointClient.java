package ru.tsc.panteleev.tm.api.endpoint;

public interface ITaskEndpointClient extends IEndpointClient, ITaskEndpoint {
}
