package ru.tsc.panteleev.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import ru.tsc.panteleev.tm.api.endpoint.IDomainEndpointClient;
import ru.tsc.panteleev.tm.dto.request.data.*;
import ru.tsc.panteleev.tm.dto.response.data.*;

@NoArgsConstructor
public class DomainEndpointClient extends AbstractEndpointClient implements IDomainEndpointClient {

    @Override
    @SneakyThrows
    public DataBackupLoadResponse loadDataBackup(DataBackupLoadRequest request) {
        return call(request, DataBackupLoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataBackupSaveResponse saveDataBackup(DataBackupSaveRequest request) {
        return call(request, DataBackupSaveResponse.class);
    }

    @Override
    @SneakyThrows
    public DataBase64LoadResponse loadDataBase64(DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataBase64SaveResponse saveDataBase64(DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @Override
    @SneakyThrows
    public DataBinaryLoadResponse loadDataBinary(DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataBinarySaveResponse saveDataBinary(DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @Override
    @SneakyThrows
    public DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(DataJsonFasterXmlLoadRequest request) {
        return call(request, DataJsonFasterXmlLoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(DataJsonFasterXmlSaveRequest request) {
        return call(request, DataJsonFasterXmlSaveResponse.class);
    }

    @Override
    @SneakyThrows
    public DataJsonJaxbLoadResponse loadDataJsonJaxb(DataJsonJaxbLoadRequest request) {
        return call(request, DataJsonJaxbLoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataJsonJaxbSaveResponse saveDataJsonJaxb(DataJsonJaxbSaveRequest request) {
        return call(request, DataJsonJaxbSaveResponse.class);
    }

    @Override
    @SneakyThrows
    public DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(DataXmlFasterXmlLoadRequest request) {
        return call(request, DataXmlFasterXmlLoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(DataXmlFasterXmlSaveRequest request) {
        return call(request, DataXmlFasterXmlSaveResponse.class);
    }

    @Override
    @SneakyThrows
    public DataXmlJaxbLoadResponse loadDataXmlJaxb(DataXmlJaxbLoadRequest request) {
        return call(request, DataXmlJaxbLoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataXmlJaxbSaveResponse saveDataXmlJaxb(DataXmlJaxbSaveRequest request) {
        return call(request, DataXmlJaxbSaveResponse.class);
    }

    @Override
    @SneakyThrows
    public DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(DataYamlFasterXmlLoadRequest request) {
        return call(request, DataYamlFasterXmlLoadResponse.class);
    }

    @Override
    @SneakyThrows
    public DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(DataYamlFasterXmlSaveRequest request) {
        return call(request, DataYamlFasterXmlSaveResponse.class);
    }
}
