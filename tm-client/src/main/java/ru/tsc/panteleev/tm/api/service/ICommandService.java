package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @Nullable
    AbstractCommand getCommandByName(String name);

    @Nullable
    AbstractCommand getCommandByArgument(String argument);

    @NotNull
    Iterable<AbstractCommand> getCommandsWithArgument();

}
