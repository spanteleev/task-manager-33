package ru.tsc.panteleev.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.data.DataXmlJaxbLoadRequest;

public class DataXmlJaxbLoadCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file";

    @NotNull
    public static final String NAME = "data-load-xml-jaxb";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        showDescription();
        getDomainEndpoint().loadDataXmlJaxb(new DataXmlJaxbLoadRequest());
    }

}
