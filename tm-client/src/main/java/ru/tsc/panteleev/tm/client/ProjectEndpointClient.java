package ru.tsc.panteleev.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.endpoint.IProjectEndpointClient;
import ru.tsc.panteleev.tm.dto.request.project.*;
import ru.tsc.panteleev.tm.dto.response.project.*;

@NoArgsConstructor
public class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpointClient {

    public ProjectEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectListResponse listProject(@NotNull ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIdResponse changeProjectStatusById(@NotNull ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectChangeStatusByIndexResponse changeProjectStatusByIndex(@NotNull ProjectChangeStatusByIndexRequest request) {
        return call(request, ProjectChangeStatusByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectCompleteByIdResponse completeProjectStatusById(@NotNull ProjectCompleteByIdRequest request) {
        return call(request, ProjectCompleteByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectCompleteByIndexResponse completeProjectStatusByIndex(@NotNull ProjectCompleteByIndexRequest request) {
        return call(request, ProjectCompleteByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectRemoveByIdResponse removeProjectById(@NotNull ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectRemoveByIndexResponse removeProjectByIndex(@NotNull ProjectRemoveByIndexRequest request) {
        return call(request, ProjectRemoveByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectShowByIdResponse showProjectById(@NotNull ProjectShowByIdRequest request) {
        return call(request, ProjectShowByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectShowByIndexResponse showProjectByIndex(@NotNull ProjectShowByIndexRequest request) {
        return call(request, ProjectShowByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectStartByIdResponse startProjectById(@NotNull ProjectStartByIdRequest request) {
        return call(request, ProjectStartByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectStartByIndexResponse startProjectByIndex(@NotNull ProjectStartByIndexRequest request) {
        return call(request, ProjectStartByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectUpdateByIdResponse updateProjectById(@NotNull ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull ProjectUpdateByIndexResponse updateProjectByIndex(@NotNull ProjectUpdateByIndexRequest request) {
        return call(request, ProjectUpdateByIndexResponse.class);
    }

}
