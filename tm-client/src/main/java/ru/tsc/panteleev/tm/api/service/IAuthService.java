package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;

public interface IAuthService {

    void login(@Nullable String login, @Nullable String password);

    void logout();

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void checkRoles(@Nullable final Role[] roles);

    @NotNull
    User check(String login, String password);
}
