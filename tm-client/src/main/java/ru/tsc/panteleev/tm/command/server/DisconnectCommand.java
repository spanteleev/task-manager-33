package ru.tsc.panteleev.tm.command.server;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.enumerated.Role;

import java.net.Socket;

public class DisconnectCommand extends AbstractCommand {

    @NotNull
    public static final String NAME = "disconnect";

    @NotNull
    public static final String DESCRIPTION = "Disconnect from server";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        try {
            serviceLocator.getConnectionEndpointClient().disconnect();
        } catch (@NotNull Exception e) {
            serviceLocator.getLoggerService().error(e);
        }
    }

    @Override
    public Role[] getRoles() {
        return null;
    }
}
