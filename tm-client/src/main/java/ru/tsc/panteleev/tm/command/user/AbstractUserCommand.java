package ru.tsc.panteleev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.IAuthEndpointClient;
import ru.tsc.panteleev.tm.api.endpoint.IUserEndpointClient;
import ru.tsc.panteleev.tm.client.AuthEndpointClient;
import ru.tsc.panteleev.tm.command.AbstractCommand;
import ru.tsc.panteleev.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void showUser(@NotNull final User user) {
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

    @NotNull
    protected IUserEndpointClient getUserEndpoint() {
        return serviceLocator.getUserEndpointClient();
    }

    @NotNull
    protected IAuthEndpointClient getAuthEndpoint() {
        return serviceLocator.getAuthEndpointClient();
    }

}
