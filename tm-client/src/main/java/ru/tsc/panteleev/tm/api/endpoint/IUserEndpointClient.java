package ru.tsc.panteleev.tm.api.endpoint;

public interface IUserEndpointClient extends IEndpointClient, IUserEndpoint {
}
