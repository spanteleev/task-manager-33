package ru.tsc.panteleev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.system.ServerVersionRequest;

public final class VersionCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "version";

    @NotNull
    public static final String DESCRIPTION = "Show application version.";

    @NotNull
    public static final String ARGUMENT = "-v";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println(serviceLocator.getSystemEndpointClient().getVersion(new ServerVersionRequest()).getVersion());
    }

}
