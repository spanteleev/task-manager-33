package ru.tsc.panteleev.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.api.endpoint.ITaskEndpointClient;
import ru.tsc.panteleev.tm.dto.request.task.*;
import ru.tsc.panteleev.tm.dto.response.task.*;

@NoArgsConstructor
public class TaskEndpointClient extends AbstractEndpointClient implements ITaskEndpointClient {

    public TaskEndpointClient(@NotNull AbstractEndpointClient client) {
        super(client);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskCreateResponse createTask(@NotNull TaskCreateRequest request) {
        return call(request, TaskCreateResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskListResponse listTask(@NotNull TaskListRequest request) {
        return call(request, TaskListResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskChangeStatusByIdResponse changeTaskStatusById(@NotNull TaskChangeStatusByIdRequest request) {
        return call(request, TaskChangeStatusByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@NotNull TaskChangeStatusByIndexRequest request) {
        return call(request, TaskChangeStatusByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskClearResponse clearTask(@NotNull TaskClearRequest request) {
        return call(request, TaskClearResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskCompleteByIdResponse completeTaskStatusById(@NotNull TaskCompleteByIdRequest request) {
        return call(request, TaskCompleteByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskCompleteByIndexResponse completeTaskStatusByIndex(@NotNull TaskCompleteByIndexRequest request) {
        return call(request, TaskCompleteByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskRemoveByIdResponse removeTaskById(@NotNull TaskRemoveByIdRequest request) {
        return call(request, TaskRemoveByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskRemoveByIndexResponse removeTaskByIndex(@NotNull TaskRemoveByIndexRequest request) {
        return call(request, TaskRemoveByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskShowByIdResponse showTaskById(@NotNull TaskShowByIdRequest request) {
        return call(request, TaskShowByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskShowByIndexResponse showTaskByIndex(@NotNull TaskShowByIndexRequest request) {
        return call(request, TaskShowByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskStartByIdResponse startTaskById(@NotNull TaskStartByIdRequest request) {
        return call(request, TaskStartByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskStartByIndexResponse startTaskByIndex(@NotNull TaskStartByIndexRequest request) {
        return call(request, TaskStartByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskUpdateByIdResponse updateTaskById(@NotNull TaskUpdateByIdRequest request) {
        return call(request, TaskUpdateByIdResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskUpdateByIndexResponse updateTaskByIndex(@NotNull TaskUpdateByIndexRequest request) {
        return call(request, TaskUpdateByIndexResponse.class);
    }

    @Override
    @SneakyThrows
    public @NotNull TaskShowListByProjectIdResponse showListByProjectIdTask(@NotNull TaskShowListByProjectIdRequest request) {
        return call(request, TaskShowListByProjectIdResponse.class);
    }

}
