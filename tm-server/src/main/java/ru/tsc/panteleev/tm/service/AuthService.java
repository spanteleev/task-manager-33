package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.service.IAuthService;
import ru.tsc.panteleev.tm.api.service.IPropertyService;
import ru.tsc.panteleev.tm.api.service.IUserService;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.exception.field.LoginEmptyException;
import ru.tsc.panteleev.tm.exception.field.PasswordEmptyException;
import ru.tsc.panteleev.tm.exception.user.AccessDeniedException;
import ru.tsc.panteleev.tm.exception.user.LoginOrPasswordIncorrectException;
import ru.tsc.panteleev.tm.exception.user.PermissionException;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.util.HashUtil;

import java.util.Arrays;

public class AuthService implements IAuthService {

    @Nullable
    private final IPropertyService propertyService;

    @Nullable
    private final IUserService userService;


    public AuthService(
            @Nullable final IPropertyService propertyService,
            @Nullable final IUserService userService
    ) {
        this.propertyService = propertyService;
        this.userService = userService;
    }

    @NotNull
    @Override
    public User check(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new LoginOrPasswordIncorrectException();
        if (user.getLocked()) throw new LoginOrPasswordIncorrectException();
        @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
        if (passwordHash == null) throw new AccessDeniedException();
        if (!passwordHash.equals(user.getPasswordHash())) throw new LoginOrPasswordIncorrectException();
        return user;
    }

    @Override
    public User registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

}
