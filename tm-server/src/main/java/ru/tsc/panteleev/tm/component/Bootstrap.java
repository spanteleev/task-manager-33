package ru.tsc.panteleev.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.api.endpoint.*;
import ru.tsc.panteleev.tm.api.repository.IProjectRepository;
import ru.tsc.panteleev.tm.api.repository.ITaskRepository;
import ru.tsc.panteleev.tm.api.repository.IUserRepository;
import ru.tsc.panteleev.tm.api.service.*;
import ru.tsc.panteleev.tm.endpoint.*;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;
import ru.tsc.panteleev.tm.repository.ProjectRepository;
import ru.tsc.panteleev.tm.repository.TaskRepository;
import ru.tsc.panteleev.tm.repository.UserRepository;
import ru.tsc.panteleev.tm.service.*;
import ru.tsc.panteleev.tm.util.DateUtil;
import ru.tsc.panteleev.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, taskRepository, projectRepository, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userService);

    @Getter
    @NotNull
    private final IDomainService domainService = new DomainService(taskService, projectService, userService);

    @NotNull
    private final IDomainEndpoint domainEndpoint = new DomainEndpoint(this);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final IProjectTaskEndpoint projectTaskEndpoint = new ProjectTaskEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final Backup backup = new Backup(this);

    {
        registry(systemEndpoint);
        registry(userEndpoint);
        registry(projectEndpoint);
        registry(projectTaskEndpoint);
        registry(taskEndpoint);
        registry(domainEndpoint);
    }

    public void registry(Object endpoint) {
        final String host = getPropertyService().getServerHost();
        final String port = getPropertyService().getServerPort().toString();
        final String name = endpoint.getClass().getSimpleName();
        final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void runStartupOperation() {
        initPID();
        backup.start();
        initDemoData();
        registryShutdownHookOperation();
        loggerService.info("** TASK-MANAGER SERVER IS STARTING **");
    }

    private void registryShutdownHookOperation() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER SERVER IS SHUTTING DOWN **");
                backup.stop();
            }
        });
    }

    private void initDemoData() {
        if (userService.getSize() > 0) return;
        userService.create("test", "test", "qwerty@qwerty.ru");
        userService.create("admin", "admin", Role.ADMIN);
        @Nullable final User admin = userService.findByLogin("admin");
        @Nullable final User test = userService.findByLogin("test");
        if (admin == null || test == null) return;
        @NotNull final String adminId = admin.getId();
        @NotNull final String testId = test.getId();
        projectService.create(adminId, "p3", "333",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        projectService.create(adminId, "p1", "111");
        projectService.create(testId, "p5", "555",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        projectService.create(testId, "p4", "444");
        projectService.create(testId, "p6", "666");
        taskService.create(adminId, "t3", "333333",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        taskService.create(adminId, "t1", "111111");
        taskService.create(testId, "t5", "555555",
                DateUtil.toDate("29.08.2021"), DateUtil.toDate("29.08.2022"));
        taskService.create(testId, "t4", "444444");
        taskService.create(testId, "t6", "666666");
    }

    public void run() {
        runStartupOperation();
    }


}
