package ru.tsc.panteleev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.enumerated.Role;
import ru.tsc.panteleev.tm.model.User;

public interface IAuthService {

    @NotNull
    User check(String login, String password);

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

}
