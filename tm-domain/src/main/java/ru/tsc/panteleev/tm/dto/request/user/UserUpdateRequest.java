package ru.tsc.panteleev.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Setter
@Getter
@NoArgsConstructor
public class UserUpdateRequest extends AbstractUserRequest {

    @Nullable
    private String firstName;

    @Nullable
    private String lastName;

    @Nullable
    private String middleName;

    public UserUpdateRequest(@Nullable String firstName, @Nullable String lastName, @Nullable String middleName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.middleName = middleName;
    }
}
