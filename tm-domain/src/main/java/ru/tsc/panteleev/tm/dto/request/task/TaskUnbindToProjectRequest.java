package ru.tsc.panteleev.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskUnbindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    @Nullable
    private String taskId;

    public TaskUnbindToProjectRequest(@Nullable String projectId, @Nullable String taskId) {
        this.projectId = projectId;
        this.taskId = taskId;
    }

}
