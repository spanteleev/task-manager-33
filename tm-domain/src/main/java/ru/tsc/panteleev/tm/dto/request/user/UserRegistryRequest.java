package ru.tsc.panteleev.tm.dto.request.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractRequest;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;


@Setter
@Getter
@NoArgsConstructor
public class UserRegistryRequest extends AbstractRequest {

    @Nullable
    private String login;

    @Nullable
    private String password;

    @Nullable
    private String email;

    public UserRegistryRequest(@Nullable String login, @Nullable String password, @Nullable String email) {
        this.login = login;
        this.password = password;
        this.email = email;
    }
}
