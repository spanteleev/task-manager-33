package ru.tsc.panteleev.tm.api.endpoint;

import ru.tsc.panteleev.tm.dto.request.user.UserLoginRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserLogoutRequest;
import ru.tsc.panteleev.tm.dto.request.user.UserProfileRequest;
import ru.tsc.panteleev.tm.dto.response.user.UserLoginResponse;
import ru.tsc.panteleev.tm.dto.response.user.UserLogoutResponse;
import ru.tsc.panteleev.tm.dto.response.user.UserProfileResponse;

public interface IAuthEndpoint {

    UserLoginResponse login(UserLoginRequest request);

    UserLogoutResponse logout(UserLogoutRequest request);

    UserProfileResponse profile(UserProfileRequest request);

}
