package ru.tsc.panteleev.tm.dto.request.task;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearRequest extends AbstractUserRequest {

}
