package ru.tsc.panteleev.tm.dto.response.user;


import lombok.NoArgsConstructor;
import ru.tsc.panteleev.tm.dto.response.user.AbstractUserResponse;
import ru.tsc.panteleev.tm.model.User;

@NoArgsConstructor
public class UserProfileResponse extends AbstractUserResponse {

    public UserProfileResponse(User user) {
        super(user);
    }

}
