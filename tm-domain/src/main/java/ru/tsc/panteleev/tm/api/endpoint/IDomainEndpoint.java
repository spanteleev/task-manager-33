package ru.tsc.panteleev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.data.*;
import ru.tsc.panteleev.tm.dto.response.data.*;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.panteleev.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(DataBackupLoadRequest request);

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(DataBackupSaveRequest request);

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(DataBase64LoadRequest request);

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(DataBase64SaveRequest request);

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(DataBinaryLoadRequest request);

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(DataBinarySaveRequest request);

    @NotNull
    @WebMethod
    DataJsonFasterXmlLoadResponse loadDataJsonFasterXml(DataJsonFasterXmlLoadRequest request);

    @NotNull
    @WebMethod
    DataJsonFasterXmlSaveResponse saveDataJsonFasterXml(DataJsonFasterXmlSaveRequest request);

    @NotNull
    @WebMethod
    DataJsonJaxbLoadResponse loadDataJsonJaxb(DataJsonJaxbLoadRequest request);

    @NotNull
    @WebMethod
    DataJsonJaxbSaveResponse saveDataJsonJaxb(DataJsonJaxbSaveRequest request);

    @NotNull
    @WebMethod
    DataXmlFasterXmlLoadResponse loadDataXmlFasterXml(DataXmlFasterXmlLoadRequest request);

    @NotNull
    @WebMethod
    DataXmlFasterXmlSaveResponse saveDataXmlFasterXml(DataXmlFasterXmlSaveRequest request);

    @NotNull
    @WebMethod
    DataXmlJaxbLoadResponse loadDataXmlJaxb(DataXmlJaxbLoadRequest request);

    @NotNull
    @WebMethod
    DataXmlJaxbSaveResponse saveDataXmlJaxb(DataXmlJaxbSaveRequest request);

    @NotNull
    @WebMethod
    DataYamlFasterXmlLoadResponse loadDataYamlFasterXml(DataYamlFasterXmlLoadRequest request);

    @NotNull
    @WebMethod
    DataYamlFasterXmlSaveResponse saveDataYamlFasterXml(DataYamlFasterXmlSaveRequest request);

}
