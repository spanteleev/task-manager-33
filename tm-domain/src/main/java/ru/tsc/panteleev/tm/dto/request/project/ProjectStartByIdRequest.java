package ru.tsc.panteleev.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.request.AbstractUserRequest;
import ru.tsc.panteleev.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
public class ProjectStartByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectStartByIdRequest(@Nullable String id) {
        this.id = id;
    }

}
