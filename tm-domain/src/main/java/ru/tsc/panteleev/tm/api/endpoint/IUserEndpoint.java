package ru.tsc.panteleev.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.panteleev.tm.dto.request.user.*;
import ru.tsc.panteleev.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @NotNull
    String NAMESPACE = "http://endpoint.tm.panteleev.tsc.ru/";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + NAME + "?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final QName qName = new QName(NAMESPACE, PART);
        return Service.create(url, qName).getPort(IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserChangePasswordResponse changeUserPassword(@NotNull UserChangePasswordRequest request);

    @NotNull
    @WebMethod
    UserLockResponse lockUser(@NotNull UserLockRequest request);

    @NotNull
    @WebMethod
    UserRegistryResponse registrationUser(@NotNull UserRegistryRequest request);

    @NotNull
    @WebMethod
    UserRemoveResponse removeUser(@NotNull UserRemoveRequest request);

    @NotNull
    @WebMethod
    UserUnlockResponse unlockUser(@NotNull UserUnlockRequest request);

    @NotNull
    @WebMethod
    UserUpdateResponse updateUserProfile(@NotNull UserUpdateRequest request);

}
